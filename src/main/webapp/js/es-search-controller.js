bankApp.controller('QueryController', function ($scope, es, Event, $websocket) {

// http://localhost:9200/test/event/_search

    $scope.channel = 'general';
    $scope.general = {count: 0};
    $scope.project = {count: 0};
    $scope.count = {};

    $scope.count['general']=0;
    $scope.count['project']=0;

    $scope.cur = 1;

    $scope.switch = function (channel) {
        $scope.channel = channel;
        $scope.show();
        $scope.count[channel] = 0;
        $scope.cur = 1;

    }

    $scope.showMore = function () {

        es.search({
            index: 'test',
            type: 'event',
            from: 11*$scope.cur,
            size: 10,
            body: {
                "query":
                {
                    "match": {
                        channel:$scope.channel
                    }
                }
                ,
                "sort": [
                    {"eventtime": "desc"}
                ]

            }

        }).then(function (response) {
            $scope.cur++;
            angular.forEach(response.hits.hits, function(item){
                // console.log(item);
                $scope.hits.push(item);
            })
        });

    };

// search for documents
    $scope.show = function () {
        es.search({
            index: 'test',
            type: 'event',
            size: 10,
            body: {
                "query": {
                    "match": {
                        channel: $scope.channel
                    }
                }
                ,
                "sort": [
                    //{ "post_date" : {"order" : "asc"}},
                    //"user",
                    {"eventtime": "desc"},
                    //{ "age" : "desc" },
                    //"_score"
                ]
            }

        }).then(function (response) {
            $scope.hits = response.hits.hits;
        });
    };

    $scope.show();

    // http://localhost:9200/test/event/
    $scope.post = function () {


        $scope.event.user = "pascal";
        $scope.event.eventtime = new Date().getTime();//"2016-02-09";
        $scope.event.title = "create ticketing service via IHM";
        $scope.event.due = "2016-03-31";
        $scope.event.channel = $scope.channel;

        Event.create($scope.event);


        $scope.event = "";

        return;

    }


    // Open a WebSocket connection
    var dataStream = $websocket('ws://localhost:9400/ws/_changes');

    var collection = [];

    dataStream.onMessage(function (message) {
        //$scope.hits.push(JSON.parse(message.data));
        //$scope.general.count++;
        msg = JSON.parse(message.data);
        // console.log('data: ' + msg);
        if (msg._source.channel != $scope.channel) {
            //$scope.count[message.data.channel].count++;
            $scope.count[msg._source.channel]++;
        } else {
            $scope.hits.splice(0, 0, msg);
        }
    });

/*
    var methods = {
        collection: collection,
        get: function () {
            dataStream.send(JSON.stringify({action: 'get'}));
        }
    };

    return methods;
*/

});
