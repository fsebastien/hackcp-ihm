// We define an bankApp module that depends on the elasticsearch module.
var bankApp = angular.module('bankApp', ['app.services' , 'app.controllers', 'ngResource', 'ngRoute',
'ui-notification', 'ui.bootstrap'])
.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
            when('/login', {
            	templateUrl:'app/components/login/login.html',
            controller:'LoginController'}).
              when('/landing', {
                	templateUrl:'app/components/landing/landing.html'}).
              when('/welcome', {
              	templateUrl:'app/components/welcome/welcome.html',
              controller:'WelcomeController' }).
              when('/virement', {
              	templateUrl:'app/components/virement/virement.html',
                controller:'VirementController'}).
              when('/compte', {
                	templateUrl:'app/components/compte/compte.html'}).
              when('/historique', {
              	templateUrl:'app/components/historique/historique.html',
              controller:'HistoriqueController' }).
            otherwise({redirectTo: '/'});
    }]);

//var services = bankApp.module('app.services', ['ngResource']);
// var controllers = angular.module('app.controllers', ['app.services']);
var controllers = angular.module('app.controllers', []);


bankApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown", function (e) {
            if (e.which === 13 && (!e.ctrlKey) && (!e.shiftKey)) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
});
