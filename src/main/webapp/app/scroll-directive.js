
bankApp.directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];

        var funCheckBounds = function(evt) {
//            console.log("event fired: " + evt.type);
            var rectObject = raw.getBoundingClientRect();
//            console.log("bottom: " + rectObject.bottom);
//            console.log("window : " + window.innerHeight);
            if (rectObject.bottom <= (window.innerHeight - 50)) {
                scope.$apply(attr.whenScrolled);
            }

        };

        angular.element(window).bind('scroll load', funCheckBounds);


    };
});
