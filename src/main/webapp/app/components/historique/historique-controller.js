controllers.controller('HistoriqueController', ['$scope',
    function ($scope ) {

      $scope.currentPage=1;

      function refresh() {
        $scope.historique.content = [
          {"date": "20 février", "libelle": "Prélèvement de ", "montant": 2000.5},
          {"date": "19 février", "libelle": "Salaire ", "montant": 2575.54}
        ];
        $scope.historique.range = {"total": 27, "from":11, "to": 15};

      }

      $scope.pageChanged = refresh;

      $scope.historique = {};

      $scope.historique.content = [
        {"date": "20 mars", "libelle": "Virement de ", "montant": 1000.5},
        {"date": "19 mars", "libelle": "Opération carte de ", "montant": 234.54}
      ];
      $scope.historique.range = {"total": 27, "from":6, "to": 10};

    }]);
