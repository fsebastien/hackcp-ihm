bankApp.factory('Compte', function ($resource) {
    return $resource('http://cacp-bank.hdp1.silca/user/:name/accounts', {}, {
//        query:      { method: 'GET', isArray: true },
        query: {uri: 'http://cacp-bank.hdp1.silca/user/:name/accounts', method: 'GET', isArray: true, params: {name: '@name'}} ,
      queryall: {uri: 'http://cacp-bank.hdp1.silca/accounts/all', method: 'GET', isArray: true} ,
       transfer:     { method: 'POST', url: 'http://cacp-bank.hdp1.silca/user/:name/account/:compte/transfer', params: {name: '@name', compte: '@compte'} } //,
//        show:       { method: 'GET',  url: 'mxamo/resource/pilotage/bindingRules/:id' },
//        remove:     { method: 'POST', url: 'mxamo/resource/pilotage/bindingRules/:id', params: {id: '@id'} },
//        activer:    { method: 'POST', url: 'mxamo/resource/pilotage/bindingRules/activer/:id', params: {id: '@id'} },
//        desactiver: { method: 'POST', url: 'mxamo/resource/pilotage/bindingRules/desactiver/:id', params: {id: '@id'}  }
    })
});
